# node-todo-api
## project structure
```
server
    ├── authentication
    │   └── auth-controller.js
    ├── config
    │   └── config.js
    ├── db
    │   └── mongoose.js
    ├── routes
    │   ├── todos.js
    │   └── users.js
    ├── server.js
    ├── tests
    │   ├── seed
    │   │   └── seed.js
    │   └── server.test.js
    ├── todos
    │   ├── todo-model.js
    │   └── todos-controller.js
    └── users
        ├── user-model.js
        └── users-controller.js
```
