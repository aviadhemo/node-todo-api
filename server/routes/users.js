const router = require('express').Router();

const userCtrl = require('./../users/users-controller');
const {authenticate} = require('./../authentication/auth-controller');

//add new user
router.post('/users', (...params) => {
    userCtrl.create(...params);
});

//get user by token
router.get('/users/me', authenticate,(...params) => {
    userCtrl.getByToken(...params);
});

module.exports = {
    router
};