const router = require('express').Router();

const Todo = require('./../todos/todo-model');
const todoCtrl = require('./../todos/todos-controller');

//list all todos
router.get('/todos', (...params) => {
    todoCtrl.list(...params);
});

//get todo by id
router.get('/todos/:id', (...params) => {
    todoCtrl.getById(...params);
});

//add new todo
router.post('/todos', (...params) => {
    todoCtrl.create(...params);
});

//delete todo
router.delete('/todos/:id', (...params) => {
    todoCtrl.removeById(...params)
});

//update todo
router.patch('/todos/:id', (...params) => {
    todoCtrl.update(...params);
});

module.exports = {
    router
};