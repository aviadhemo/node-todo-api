const validator = require('validator');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

//create User schema
const UserSchema = new Schema({
    email: {
        type: String,
        required: [true, 'email field is required'],
        trim: true,
        minlength: 5,
        unique: true,
        validate: {
            isAsync: true,
            validator: validator.isEmail
        }
    },
    password: {
        type: String,
        required: [true, 'password field is required'],
        minlength: 6
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
}, {usePushEach: true});

//generate jwt token
UserSchema.methods.generateAuthToken = function() {
    let user = this;
    let access = 'auth';
    let token = jwt.sign({_id: user._id.toHexString(), access}, 'abc123').toString();
    
    user.tokens.push({access, token});
    
    return user.save().then(() => {
        return token;
    });
};

//override toJSON to pick properties of object to be sent
UserSchema.methods.toJSON = function() {
    const user = this;
    const userObject = user.toObject();
    
    return _.pick(userObject, ['_id', 'email']);
};

UserSchema.statics.findByToken = function(token) {
    let User = this;
    let decoded;
    
    try {
        decoded = jwt.verify(token, 'abc123');
    } catch(err) {
        return Promise.reject(err);
    }
    
    return User.findOne({
        '_id': decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    });
};

//hash password before saving user doc
UserSchema.pre('save', function(next) {
    let User = this;
    
    if(user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();                
            });
        }) ;
    } else {
        next();
    }
});

UserSchema.statics.findByCredentials = function(email, password) {
    const user = this;

    return user.findOne({email}).then((user) => {
        if(!user) {
            return Promise.reject();
        }

        return bcrypt.compare(password, user.password);
    });
};

//create user model
const User = mongoose.model('User', UserSchema);

module.exports = {
    User
};