const _ = require('lodash');
const {User} = require('./user-model');

//create new user
function create(req, res, next) {
    const body = _.pick(req.body, ['email', 'password']);
    const user = new User(body);

    user.save().then(() => {
        return user.generateAuthToken();
    }).then((token) => {
        res.header('x-auth', token).send(user);
    })
    .catch((err) => res.status(400).send(err));
}

function getByToken(req, res, next) {
    const token = req.header('x-auth');

    User.findByToken(token)
        .then((user) => {
            if(!user) {
                return Promise.reject();
            }

            res.send(user);
        }).catch((err) => {
            res.status(401).send();
        });
}

module.exports = {
    create,
    getByToken
};