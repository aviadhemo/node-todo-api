const {User} = require('./../users/user-model');
const _ = require('lodash');

const authenticate = (req, res, next) => {
    let token = req.header('x-auth');

    User.findByToken(token).then((user) => {
        if(!user) {
            return Promise.reject();
        }

        req.user = user;
        req.token = token;
        next();
    }).catch((err) => {
        res.status(401).send();
    });
};

const login = (req, res, next) => {
    const body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email, body.password)
        .then((user) => {
            res.send(user);
        }).catch((err) => {
            res.status(400).send();
        });
};

module.exports = {
    authenticate,
    login
};