require('./config/config');

const express = require('express');
const bodyParser = require('body-parser');

const { mongoose } = require('./db/mongoose');
const { Todo } = require('./todos/todo-model');
// const { User } = require('./users/user-model');
const todosRoutes = require('./routes/todos').router;
const usersRoutes = require('./routes/users').router;
const PORT = process.env.PORT;
const app = express();

const authCtrl = require('./authentication/auth-controller');

app.use(bodyParser.json());

// app.post('/api/users/login', authCtrl.login);

app.use('/api', usersRoutes);
app.use('/api', todosRoutes);

//error handling middleware
app.use((err, req, res, next) => {
    res.status(422)
        .send({ error: err.message})
});

app.listen(PORT, () => {
    console.log(`Server running on port ${ PORT }`);
});

module.exports = {
    app
};