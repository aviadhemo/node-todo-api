const env = process.env.NODE_ENV || 'development';

console.log('env ****', env);

if(env === 'development') {
    process.env.PORT = 3000;
    process.env.MONGODB_URI = 'mongodb://localhost:27017/TodoApp';
} else if(env === 'test') {
    process.env.PORT = 3000;
    process.env.MONGODB_URI = 'mongodb://localhost:27017/TodoAppTest';
}

const config = {
    env,
    port: process.env.PORT,
    db: {
        host: process.env.MONGODB_URI
    }
};

module.exports = { config };