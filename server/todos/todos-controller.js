const _ = require('lodash');
const {ObjectID} = require('mongodb');

const Todo = require('../todos/todo-model').Todo;

function list(req, res, next) {
    Todo.find()
        .then(todos => {
            res.send({ todos });
        }).catch((err) => {
            next(err);
        });
}

function create(req, res, next) {
    let todo = new Todo({
        text: req.body.text
    });

    todo.save().then((todo) => {
        res.send(todo);
    }).catch((err) => {
        next(err);
    });
}

function removeById(req, res, next) {
    let id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404);
    }

    Todo.findByIdAndRemove(id)
        .then((todo) => {
            if(!todo) {
                return next();
            }

            res.send({todo});
        }).catch((err) => next(err));
}

function getById(req, res, next) {
    let id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404);
    }

    Todo.findById(id)
        .then((todo) => {
            if(!todo) {
                return next();
            }

            res.send({todo})
        }).catch((err) => {
            next(err);
        });
    
}

function update(req, res, next) {
    let id = req.params.id;
    let body = _.pick(req.body, ['text', 'completed']);

    if(!ObjectID.isValid(id)) {
        return res.status(404);
    }

    if(_.isBoolean(body.completed) && body.completed) {
        body.completedAt = new Date().getTime();
    } else {
        body.completed = false;
        body.completedAt = null;
    }

    Todo.findByIdAndUpdate(id, {$set: body}, {new: true}).then((todo) => {
        if(!todo) {
            return res.status(404).send();
        }

        res.send({todo});
    }).catch(err => res.status(400).send());
    
}

module.exports = {
    list,
    create,
    getById,
    removeById,
    update
}