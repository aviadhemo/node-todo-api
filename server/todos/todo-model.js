const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create todo schema
const TodoSchema = new Schema({
    text: {
        type: String,
        required: [true, 'Todo text field is required'],
        minlength: 1,
        trim: true
    },
    completed: {
        type: Boolean,
        default: false
    },
    completedAt: {
        type: Number,
        default: null
    }
});

const Todo = mongoose.model('Todo', TodoSchema);

module.exports = {
    Todo
};